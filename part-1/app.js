// Solo de que el archivo este al mismo nivel de las carpetas
// const multiplicion = require('node/Tablas/multiplicar.js');
// const multiplicion = require('C:/Users/Oscar Rodriguez/Documents/Oscar/Practicas/node/Tablas/multiplicar');


//Forma de destruccturacion
const { CrearArchivo } = require('C:/Users/Oscar Rodriguez/Documents/Oscar/Practicas/node/Tablas/multiplicar.js');

// let base = '12';

// console.log(multiplicion);
// console.log(process.argv);

//split sirve para separar elementos
let argumento = process.argv;
let parametro = argumento[2];
let base = parametro.split('=')[1]

// console.log(base);

CrearArchivo(base)
    .then(archivo => console.log(`Archivo creado ${ archivo }`)
        .catch(e => console.log(e)));