const AWS = require('aws-sdk');
const BaseDynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {
  console.log("datos", event);
  //Consulta
  let Consulta = {
    TableName: "TiendaLiver",
    Limit: 100
  };
  
  BaseDynamo.scan(Consulta, function(err, data){
    if (err) {
      callback(err, null)
    } else {
      callback(null, data.Items)
  }
  });
};