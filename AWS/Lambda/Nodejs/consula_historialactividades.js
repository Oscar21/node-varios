const AWS = require('aws-sdk');
const BaseDynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {
      var usuariodato = event;
      var lista = [];
    
    var idUsuario = usuariodato.idUsuario;
    //var idUsuario = "297956a4-22b3-47f9-b897-9a7007cd649d";
  
  console.log("datos", event);
  //Consulta
  let Consulta = {
    TableName: "HistorialActividades",
    Key:{
      idUsuario : "idUsuario"
    }
  };

  var params = {
    ExpressionAttributeValues: {
      ":idUsuario": idUsuario
    },
        FilterExpression: "idUsuario = :idUsuario",
        TableName: "HistorialActividades"
    };
  
    BaseDynamo.scan(params,(err, data)=>
    {
        if (err) {
          console.error(err);
          callback(null, {
            statusCode: err.statusCode || 501,
            headers: { 'Content-Type': 'application/json' },
            status: false,
            body: 'No se pudo realizar la consulta.',
          });
          return;
        }else{
          var algo = [];
          for(var i = 0; i < data.Count; -1, i++){
            console.log(i);
            algo.push(data.Items[i].Actividades)
          }
          //console.log(algo); 
          
          let arrayActividades = data.Items[0].Actividades;
          
          lista = {
            "nombre": data.Items[0].nombre,
            "idhistorial": data.Items[0].idhistorial,
            "idUsuario": data.Items[0].idUsuario, 
            "apellidoPaterno": data.Items[0].apellidoPaterno,
            "apellidoMaterno":data.Items[0].apellidoMaterno,
            "Actividades": algo
          };
          const response = {
            statusCode: 200,
            body: lista,
            message: ""
            
          };
          callback(null, response);
        }
  }
  );
  
  
};

