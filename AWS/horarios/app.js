const moment = require('moment-timezone');
moment().tz("America/Los_Angeles").format();

let horaA = new Date().getTime();
let a = moment(horaA).format('DD/MM/YYYY, h:mm:ss');

let b = moment(horaA).add(8, 'hours').format('DD/MM/YYYY, h:mm:ss');

console.log(a);
console.log(b);