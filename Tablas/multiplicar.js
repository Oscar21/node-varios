const fs = require('fs');

let CrearArchivo = (base) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject(`El valor introducido ${base} no es numerico`);
            return;
        }


        let data = '';

        for (let i = 1; i <= 1000; i++) {
            data += `${ base } * ${i} = ${ base * i } \n`;
        }

        fs.writeFile(`tabla-${ base }.txt`, data, (err) => {
            // if (err) throw err;
            // console.log(`El archivo tabla-${ base }.txt ha sido creado`);
            if (err) reject(err)
            else
                resolve(`tabla-${ base }.txt`);
        });
    });
}

module.exports = {
    // se puede poner de estas dos formas
    // CrearArchivo : CrearArchivo
    CrearArchivo
}

//*****************************************************************************++ */
// Tambien se puede realziar así, pero si se tiene muchas funciones se puede exportar de la forma de arriba

// module.exports.CrearArchivo = (base) => {
//     return new Promise((resolve, reject) => {

//         let data = '';

//         for (let i = 1; i <= 10; i++) {
//             data += `${ base } * ${i} = ${ base * i } \n`;
//         }

//         fs.writeFile(`part-1/tabla-${ base }.txt`, data, (err) => {
//             // if (err) throw err;
//             // console.log(`El archivo tabla-${ base }.txt ha sido creado`);
//             if (err) reject(err)
//             else
//                 resolve(`tabla-${ base }.txt`);
//         });
//     });
// }