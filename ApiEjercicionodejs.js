var AWS = require('aws-sdk');

AWS.config.region = 'us-east-1';
var api_var = new AWS.APIGateway({apiVersion: '2015/07/09'});

// api_var.createRestApi({
//     name: "TiendaPet_Osc",
//     binaryMediaTypes: ['*'],
//     description: "Demo de un api usando aws sdk de nodejs",
//     version: "0.00.001"},
//     function(err,data){
//         if (!err)
//         {
//             console.log(data);
//         }
//         else{
//             console.log('La creacion del api fallo: \n', err);
//         }
//     }
//     );

//     /*
// { id: 'psz8lzhi0e',
//   name: 'TiendaPet_Osc',
//   description: 'Demo de un api usando aws sdk de nodejs',
//   createdDate: 2019-07-04T22:32:05.000Z,
//   version: '0.00.001',
//   binaryMediaTypes: [ '*' ],
//   apiKeySource: 'HEADER',
//   endpointConfiguration: { types: [ 'EDGE' ] } }
//     */

// api_var.getResources(
//     {
//         restApiId: 'psz8lzhi0e'
//     },
//     function(err, data){
//         if(!err){
//             console.log(data);
//         }
//         else{
//             console.log('La creacion del api fallo: \n', err);
//         }
//     }
// )

     /*
     { 
         items: 
         [ 
             { 
                 id: '825zlhl1j3', 
                 path: '/' 
                } 
            ] 
        }     
        */

// api_var.createResource(
//     {
//         restApiId: 'psz8lzhi0e',
//         parentId: '825zlhl1j3',
//         pathPart: 'mascotas',
//     },
//     function(err,data){
//         if(!err){
//             console.log(data);
//         }else{
//             console.log('/mascotas fallo al crearse', err);
//         }
//     }
// )

/*
{ 
    id: 'qe5kh2',
    parentId: '825zlhl1j3',
    pathPart: 'mascotas',
    path: '/mascotas' }
  */

//  api_var.createResource(
//      {
//          restApiId: 'psz8lzhi0e',
//          parentId: 'qe5kh2',
//          pathPart: '{mascotaId}'
//      },
//      function(err,data){
//          if(!err){
//              console.log(data);
//          }else{
//              console.log('/mascotas/mascotaId fallo en crearse', err);
//          }
//      }
//  )

 /*
 { 
     id: 'mlscmf',
     parentId: 'qe5kh2',
     pathPart: '{mascotaId}',
     path: '/mascotas/{mascotaId}' 
    }
 */

//  api_var.putMethod(
//      {
//          restApiId: 'psz8lzhi0e',
//          resourceId: 'mlscmf',
//          httpMethod: 'GET',
//          authorizationType: 'NONE'
//         },function(err, data){
//             if(!err){
//                 console.log(data);
//             }
//             else{
//                 console.log('El metodo Get/mascotas fallo al crearse', err);
//             }
//         }
// )

/*
{ 
    httpMethod: 'GET',
    authorizationType: 'NONE',
    apiKeyRequired: false 
}
*/

//Para este metodo se borro el get que ya se tenia
// api_var.putMethod({
// 	restApiId: 'psz8lzhi0e',
// 	resourceId: 'mlscmf',
// 	httpMethod: 'GET',
// 	authorizationType: 'NONE',
//       requestParameters: {
//         "method.request.path.petId" : true
//       }
// }, function(err, data){
// 	if (!err) {
// 		console.log(data);
// 	} else {
// 		console.log("El metodo Get/mascotas fallo al crearse:\n", err);
// 	}	
// })

/*
{ 
    httpMethod: 'GET',
    authorizationType: 'NONE',
    apiKeyRequired: false,
    requestParameters: 
    { 
        'method.request.path.mascotaId': true 
    } 
}
*/


/*
{ 
    id: 'qe5kh2',
    parentId: '825zlhl1j3',
    pathPart: 'mascotas',
    path: '/mascotas' }
  */


// api_var.putMethodResponse({
// 	restApiId: 'psz8lzhi0e',
// 	resourceId: "qe5kh2",
// 	httpMethod: 'GET',
// 	statusCode: '200'
// }, function(err, data){
// 	if (!err) {
// 		console.log(data);
// 	} else {
// 		console.log("Set up the 200 OK response for the 'GET /pets' method failed:\n", err);
// 	}
// })



// api_var.putMethodResponse({
//     restApiId: 'psz8lzhi0e',
//     resourceId: "mlscmf",
//     httpMethod: 'GET',
//     statusCode: '200',
// }, function(err, data){
//     if(!err){
//         console.log(data);
//     }else{
//         console.log('No se configuro la respuesta 200 OK para el metodo GET: \n', err);
//     }
// })

/*
{ statusCode: '200' }
*/

// api_var.putIntegration({
//     restApiId: 'psz8lzhi0e',
//     resourceId: "mlscmf",
//     httpMethod: 'GET',
//     type: 'HTTP',
//     integrationHttpMethod: 'GET',
//     uri: 'http://mascota-demo-endpoint.execute-api.com/mascotas',
// }, function(err,data){
//     if(!err){
//         console.log(data);
//     }
//     else
//     {
//         console.log('Error en el putIntegration: \n', err);
//     }
// })

/*
{ type: 'HTTP',
  httpMethod: 'GET',
  uri: 'http://mascota-demo-endpoint.execute-api.com/mascotas',
  connectionType: 'INTERNET',
  passthroughBehavior: 'WHEN_NO_MATCH',
  timeoutInMillis: 29000,
  cacheNamespace: 'mlscmf',
  cacheKeyParameters: [] }
*/

// api_var.putIntegration({
// 	restApiId: 'psz8lzhi0e',
// 	resourceId: 'mlscmf',
// 	httpMethod: 'GET',
// 	type: 'HTTP',
// 	integrationHttpMethod: 'GET',
// 	uri: 'http://mascota-demo-endpoint.execute-api.com/mascotas/{id}',
//        requestParameters: {
//           "integration.request.path.id": "method.request.path.mascotaId"
//        }
// }, function(err, data){
// 	if (!err) {
// 		console.log(data);
// 	} else {
// 		console.log("The 'GET /pets/{petId}' method integration setup failed:\n", err);
// 	}
// })


/*
{ type: 'HTTP',
  httpMethod: 'GET',
  uri: 'http://mascota-demo-endpoint.execute-api.com/mascotas/{id}',
  connectionType: 'INTERNET',
  requestParameters:
   { 'integration.request.path.id': 'method.request.path.mascotaId' },
  passthroughBehavior: 'WHEN_NO_MATCH',
  timeoutInMillis: 29000,
  cacheNamespace: 'mlscmf',
  cacheKeyParameters: [] }
*/

// api_var.putIntegrationResponse({
// 	restApiId: 'psz8lzhi0e',
// 	resourceId: 'mlscmf',
// 	httpMethod: 'GET',
// 	statusCode: '200',
// 	selectionPattern: ''
// }, function(err, data){
// 	if (!err) {
// 		console.log(data);
// 	} else
// 		console.log("The 'GET /pets' method integration response setup failed:\n", err);
// })

/*
{ statusCode: '200', selectionPattern: '' }
*/


// api_var.putIntegrationResponse({
// 	restApiId: 'psz8lzhi0e',
// 	resourceId: "qe5kh2",
// 	httpMethod: 'GET',
// 	statusCode: '200',
// 	selectionPattern: ''
// }, function(err, data){
// 	if (!err) {
// 		console.log(data);
// 	} else
// 		console.log("The 'GET /pets' method integration response setup failed:\n", err);
// })

// api_var.testInvokeMethod({
// 	restApiId: 'psz8lzhi0e',
// 	resourceId: 'qe5kh2',
// 	httpMethod: "GET",
// 	pathWithQueryString: '/'
// }, function(err, data){
// 	if (!err) {
// 		console.log(data)
// 	} else {
// 		console.log("Test-invoke-method on 'GET /pets' failed: \n", err);
// 	}
// })


api_var.createDeployment({
	restApiId: 'psz8lzhi0e',
	stageName: 'test',
	stageDescription: 'test deployment',
	description: 'API deployment'
}, function(err, data){
	if (err) {
		console.log('Fallo el deploy:\n', err);
	}	else {
		console.log("Deploying API \n", data);
	}
})
